package pi.compute.utils.threading;

import java.util.concurrent.ThreadLocalRandom;

import static pi.compute.utils.Utils.CheckIfPointIsInCircle;

/**
 * Created by adrian on 10/24/16.
 */
public class Runner implements Runnable {
    final long maxPoints;
    int id;
    static private int nextID = 0;
    int pointsInCircle;

    public Runner(long maxPoints){
        this.maxPoints = maxPoints;
        pointsInCircle = 0;
        this.id = nextID;
        increaseID();
    }
    synchronized static void increaseID(){
        nextID++;
    }

    public int getPointsInCircle() {
        return pointsInCircle;
    }

    private void calculatePi(){
        try {
            for (int i=0;i<maxPoints;i++){
                double x = ThreadLocalRandom.current().nextDouble();
                double y = ThreadLocalRandom.current().nextDouble();
                if (CheckIfPointIsInCircle(x, y)) {
                    pointsInCircle++;
                }
            }
        }
        finally {
            System.out.println("Thread " + id  + " "+ this + ": done");
        }

    }
    @Override
    public void run(){
        System.out.println("Starting thread " + id + ": " + this);
        calculatePi();
    }
}
