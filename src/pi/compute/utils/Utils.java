package pi.compute.utils;

import static java.lang.Math.pow;

/**
 * Created by adrian on 10/26/16.
 */
public class Utils {
    public static boolean CheckIfPointIsInCircle(double x, double y) {
        return (pow(x, 2d) + pow(y, 2d)) <= 1d;
    }
}
