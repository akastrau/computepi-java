package pi.compute.tests;

/**
 * Created by adrian on 10/26/16.
 */
public class Tests {
    public static void main(String[] args) {
        long[] pointsIn = {627983, 628861, 628464, 628444};
        long[] inTotal = {800000, 800000, 800000, 800000};
        double[] executionTimes = {0.052, 0.048, 0.053, 0.049};

        double pi = 0;
        double error = 0;

        for (int i=0;i<pointsIn.length;i++){
            pi = 4d * ((double)pointsIn[i] / (double)inTotal[i]);
            error = Math.abs(pi - Math.PI) * 100;
            System.out.println("Statistics for i=" + i +"\n" + "PI: " + pi + "\nError: " + error + "%\nExecution time: " + executionTimes[i] + "s");
        }
    }
}
