package pi.compute.compute;


import pi.compute.utils.threading.Runner;

import java.math.BigInteger;

import static java.lang.Math.PI;
import static java.lang.Math.abs;

/**
 * Created by adrian on 10/24/16.
 */
public class Main {
    private static double pi = 0d;
    private static int numberOfProcs = Runtime.getRuntime().availableProcessors();
    private static long numberOfPointsForEachThread = 230584300; //922337203


    private static BigInteger numberOfPointsInCircle = BigInteger.valueOf(0);
    private static BigInteger totalNumberOfPoints;

    public static void main(String[] args) {
        System.out.println("Counting pi...");
        totalNumberOfPoints = BigInteger.valueOf(numberOfPointsForEachThread * numberOfProcs);
        System.out.println("Number of threads: " + numberOfProcs + ", total number of points: " + totalNumberOfPoints);
        System.out.println("Starting " + numberOfProcs + " threads...");
        Thread[] threads = new Thread[numberOfProcs];
        Runner[] slaves = new Runner[numberOfProcs];

        for (int i = 0;i<numberOfProcs;i++){
            slaves[i] = new Runner(numberOfPointsForEachThread);
        }

        for (int i=0;i<numberOfProcs; i++){
            threads[i] = new Thread(slaves[i]);
        }
        long startTime = System.currentTimeMillis();
        for (int i=0;i<numberOfProcs;i++){
            threads[i].start();
        }
        for (int i=0;i<numberOfProcs;i++){
            try {
                threads[i].join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.print("Collecting data from threads...");
        for (int i=0;i<numberOfProcs;i++){
            numberOfPointsInCircle = numberOfPointsInCircle.add(BigInteger.valueOf(slaves[i].getPointsInCircle()));
        }

        pi = 4d * (numberOfPointsInCircle.doubleValue() / totalNumberOfPoints.doubleValue());
        double error = abs((pi - PI) * 100);
        long totalExecutionTime = System.currentTimeMillis() - startTime;

        System.out.println(" done!\nPi: " + pi + "\nError: " + error + "%" + "\nExecution time: " + (double)totalExecutionTime / 1000d + "s");
    }

}



